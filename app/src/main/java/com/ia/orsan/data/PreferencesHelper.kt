package com.ia.orsan.data

import android.content.Context
import android.content.SharedPreferences

class PreferencesHelper(context: Context) {

    companion object {
        private val PREFS_FILENAME = "orsan.preferences"
        val LOGGED = "logged"
        val TUTORIAL_WAS_SHOWN = "tutorialWasShown"
        val USER_NAME = "userName"
        val USER_EMAIL = "userEmail"
        val USER_AVATAR_URL = "userAvatarUrl"
        val IS_FACEBOOK_LOGIN = "facebookLogin"
        val TOKEN = "token"
        val POSTAL_CODE = "postalCode"
        val LOCATION_ALLOWED = "locationAllowed"
        val IS_FIRST_TIME = "isFirstTime"
    }

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)
    private val editor: SharedPreferences.Editor = prefs.edit()

    fun getBoolena(preference: String, defValue: Boolean) = prefs.getBoolean(preference, defValue)

    fun setBoolean(preference: String, newValue: Boolean) {
        editor.putBoolean(preference, newValue).apply()
    }

    fun getString(preference: String, defValue: String) = prefs.getString(preference, defValue)

    fun setString(preference: String, newValue: String) {
        editor.putString(preference, newValue).apply()
    }

    fun clearInfoByLogout() {
        editor.remove(LOGGED)
        editor.remove(USER_AVATAR_URL)
        editor.remove(USER_NAME)
        editor.remove(TUTORIAL_WAS_SHOWN)
        editor.remove(USER_EMAIL)
        editor.apply()
    }
}