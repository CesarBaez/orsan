package com.ia.orsan.ui.home

import com.ia.commons.extensions.base.BaseActivity
import com.ia.orsan.R

class HomeActivity : BaseActivity() {

    override fun getLayoutResId(): Int = R.layout.activity_home

}
