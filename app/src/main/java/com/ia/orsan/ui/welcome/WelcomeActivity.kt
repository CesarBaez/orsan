package com.ia.orsan.ui.welcome

import android.content.Intent
import android.support.v4.app.Fragment
import com.ia.commons.extensions.base.BaseActivity
import com.ia.orsan.R
import com.ia.orsan.ui.adapters.WelcomeAdapter
import com.ia.orsan.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : BaseActivity() {

    private lateinit var adapter: WelcomeAdapter

    override fun getLayoutResId(): Int = R.layout.activity_welcome

    override fun initView() {
        super.initView()
        initViewPager()
        this.btn_register.setOnClickListener({ startActivity(Intent(this, RegisterActivity::class.java)) })
    }

    private fun initViewPager() {
        val list: List<Fragment> = listOf(
                WelcomeInfoFragment.newInstance(getString(R.string.first_slide_title), getString(R.string.first_slide_info)),
                WelcomeInfoFragment.newInstance(getString(R.string.second_slide_title), getString(R.string.second_slide_info)),
                WelcomeInfoFragment.newInstance(getString(R.string.third_slide_title), getString(R.string.third_slide_info)),
                WelcomeInfoFragment.newInstance(getString(R.string.fourth_slide_title), getString(R.string.fourth_slide_info))
        )
        adapter = WelcomeAdapter(supportFragmentManager, list)
        this.viewPager.adapter = adapter
        this.pageIndicatorView.setViewPager(this.viewPager)
    }

}
