package com.ia.orsan.ui.register

import android.app.ProgressDialog
import android.location.LocationManager
import android.os.Build
import android.support.annotation.RequiresApi
import com.facebook.FacebookSdk
import com.ia.commons.extensions.base.BaseActivity
import com.ia.orsan.R
import com.ia.orsan.data.PreferencesHelper
import com.pawegio.kandroid.textWatcher
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.content_register.*

class RegisterActivity : BaseActivity() {

    companion object {
        const val PERMISSIONS_REQUEST_LOCATION = 99
        const val LOCATION_ACCURACY = 50
    }

    private lateinit var dialog: ProgressDialog
    private val prefs: PreferencesHelper by lazy { PreferencesHelper(this) }
    //val locationManager:LocationManager by lazy { LocationManager(this,this,getString(R.string.getting_location)) }

    override fun getLayoutResId(): Int = R.layout.activity_register

    override fun initView() {
        super.initView()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            onPrepareSupportActionBar()
        }
        addFieldWatchers()

    }

    override fun initFacebookSdk() {
        super.initFacebookSdk()
       // FacebookSdk.sdkInitialize(applicationContext)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun onPrepareSupportActionBar() {
        this.toolbar.title = ""
        setSupportActionBar(this.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        this.toolbar.navigationIcon = getDrawable(R.drawable.ic_arrow_back_white_24dp)
        this.toolbar.setNavigationOnClickListener({ onBackPressed() })
    }

    private fun addFieldWatchers() {
        this.et_name.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
        this.et_email.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
        this.et_password.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
        this.et_postal_code.textWatcher { onTextChanged { text, start, before, count -> activeRegisterButton() } }
    }

    private fun activeRegisterButton() {
        if (!this.et_name.text.toString().isEmpty() &&
                !this.et_email.text.toString().isEmpty() &&
                !this.et_password.text.toString().isEmpty() &&
                !this.et_postal_code.text.isEmpty()) {
            this.btn_register.isEnabled = true
        } else {
            this.btn_register.isEnabled = false
        }
    }

}
