package com.ia.orsan.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.ia.commons.extensions.base.BaseActivity
import com.ia.orsan.R
import com.ia.orsan.data.PreferencesHelper
import com.ia.orsan.ui.home.HomeActivity
import com.ia.orsan.ui.welcome.WelcomeActivity

class SplashActivity : BaseActivity() {

    private val prefs: PreferencesHelper by lazy { PreferencesHelper(this) }

    override fun getLayoutResId(): Int = 0

    override fun initView() {
        super.initView()
        manageIntent()
    }

    private fun manageIntent() {
        if (prefs.getBoolena(PreferencesHelper.LOGGED, false)) {
            startActivity(Intent(this, HomeActivity::class.java))
        } else {
            startActivity(Intent(this, WelcomeActivity::class.java))
            finish()
        }
    }
}
