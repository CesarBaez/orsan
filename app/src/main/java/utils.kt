import android.support.design.widget.TextInputLayout
import android.text.TextUtils
import android.widget.EditText

class utils {
    companion object {


        fun isEmailValid(container: TextInputLayout, editText: EditText, wrongFormatMessage: String, noEmptyMessage: String): Boolean {
            var response = false
            val email = editText.text.toString()

            when (true) {
                TextUtils.isEmpty(email) -> {
                    toggleTextInputLayoutError(container, noEmptyMessage)
                }
                !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() -> {
                    toggleTextInputLayoutError(container, wrongFormatMessage)
                }
                else -> {
                    response = true
                }
            }
            return response
        }

        private fun toggleTextInputLayoutError(container: TextInputLayout, msg: String) {
            container.error = msg
            container.isErrorEnabled = true
        }
    }
}