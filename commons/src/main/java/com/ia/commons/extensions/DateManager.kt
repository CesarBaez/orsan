package com.ia.commons.extensions

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by jramos on 3/9/2018.
 */
object DateManager {

    const val DATE_FORMAT = "dd MMM yyyy"
    private const val MONTH_FORMAT = "MMM"
    private const val DAY_FORMAT = "dd"

    private val locale: Locale = Locale.getDefault()

    private val dateFormatter = SimpleDateFormat(DATE_FORMAT, locale)
    private val monthFormatter = SimpleDateFormat(MONTH_FORMAT, locale)
    private val dayFormatter = SimpleDateFormat(DAY_FORMAT, locale)

    fun formatDate(date: Date?): String {
        return if (date != null) {
            dateFormatter.format(date)
        } else {
            ""
        }
    }

    fun parseString(source: String): Date? {
        return try {
            if (source.isNotBlank()) {
                dateFormatter.parse(source)
            } else {
                null
            }
        } catch (e: ParseException) {
            null
        }
    }

    fun formatMonth(date: Date?): String {
        return if (date != null) {
            monthFormatter.format(date)
        } else {
            ""
        }
    }

    fun getDayOfTheMonth(date: Date?): String {
        return if (date != null) {
            dayFormatter.format(date)
        } else {
            ""
        }
    }
}