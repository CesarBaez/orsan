package com.ia.commons.extensions.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.ia.commons.R

/**
 * Created by rsandoval on 27/02/2018.
 */
abstract class BaseActivity : AppCompatActivity() {

    abstract fun getLayoutResId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.left_in, R.anim.left_out)
        super.onCreate(savedInstanceState)
        initFacebookSdk()
        if (getLayoutResId() != 0) {
            setContentView(getLayoutResId())
        }
        initView()

    }
    open fun initFacebookSdk(){

    }

    open fun initView() {

    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.rigth_in, R.anim.rigth_out)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                this.onBackPressed()
                true
            }
            else -> false
        }

    }

}