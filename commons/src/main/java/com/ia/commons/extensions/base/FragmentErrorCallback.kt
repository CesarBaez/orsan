package com.ia.commons.extensions.base

interface FragmentErrorCallback {
    fun onShowError(fragmentPosition: Int, errorViewHeight: Int)
    fun onDismissError(fragmentPosition: Int)
}